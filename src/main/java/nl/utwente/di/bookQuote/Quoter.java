package nl.utwente.di.bookQuote;

import java.util.HashMap;

/***
 * The class for quoter
 */
public class Quoter {

    HashMap<String, Double> priceMap;

    public Quoter(){
        priceMap = new HashMap<>();
        priceMap.put("1", 10.0);
        priceMap.put("2", 45.0);
        priceMap.put("3", 20.0);
        priceMap.put("4", 35.0);
        priceMap.put("5", 50.0);
    }

    double getBookPrice(String isbn){
        return priceMap.getOrDefault(isbn, 0.0);
    }
}
